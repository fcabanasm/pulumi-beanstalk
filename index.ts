import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

const config = new pulumi.Config();
const appName = config.require("appName");
const appEnvironment = config.require("appEnvironment");
const bucketName = config.require("bucketName");

// S3
export const bucket = new aws.s3.Bucket(bucketName);

// ECR
export const ecr = new aws.ecr.Repository(`${appName}`, {
  name: `${appName}`,
});

// Setup IamInstanceProfile
export const instanceProfileRole = new aws.iam.Role(`${appName}-eb-ec2-role`, {
  name: `${appName}-eb-ec2-role`,
  description: "Role for EC2 managed by EB",
  assumeRolePolicy: JSON.stringify({
    Version: "2012-10-17",
    Statement: [
      {
        Action: "sts:AssumeRole",
        Principal: {
          Service: "ec2.amazonaws.com",
        },
        Effect: "Allow",
        Sid: "",
      },
    ],
  }),
});

// Attach the policies for the IAM Instance Profile
const rolePolicyAttachment_ec2 = new aws.iam.RolePolicyAttachment(
  `${appName}-role-policy-attachment-ec2-ecr`,
  {
    role: instanceProfileRole.name,
    policyArn: "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
  }
);

const rolePolicyAttachment_web = new aws.iam.RolePolicyAttachment(
  `${appName}-role-policy-attachment-web`,
  {
    role: instanceProfileRole.name,
    policyArn: "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier",
  }
);

const rolePolicyAttachment_worker = new aws.iam.RolePolicyAttachment(
  `${appName}-role-policy-attachment-worker`,
  {
    role: instanceProfileRole.name,
    policyArn: "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier",
  }
);

export const instanceProfile = new aws.iam.InstanceProfile(
  `${appName}-eb-ec2-instance-profile`,
  {
    role: instanceProfileRole.name,
  }
);

// Setup ServiceRole
export const serviceRole = new aws.iam.Role(
  `${appName}-elasticbeanstalk-service-role`,
  {
    name: `${appName}-elasticbeanstalk-service-role`,
    description: "Role trusted by Elastic Beanstalk",
    assumeRolePolicy: JSON.stringify({
      Version: "2012-10-17",
      Statement: [
        {
          Action: "sts:AssumeRole",
          Condition: {
            StringEquals: {
              "sts:ExternalId": "elasticbeanstalk",
            },
          },
          Principal: {
            Service: "elasticbeanstalk.amazonaws.com",
          },
          Effect: "Allow",
          Sid: "",
        },
      ],
    }),
  }
);

// Attach the policies for the Service Role
const rolePolicyAttachment_ebHealth = new aws.iam.RolePolicyAttachment(
  `${appName}-role-policy-attachment-eb-enhanced-health`,
  {
    role: serviceRole.name,
    policyArn:
      "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth",
  }
);

const rolePolicyAttachment_ebService = new aws.iam.RolePolicyAttachment(
  `${appName}-role-policy-attachment-eb-service`,
  {
    role: serviceRole.name,
    policyArn:
      "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService",
  }
);

// Elastic Beanstalk Application
export const app = new aws.elasticbeanstalk.Application(`${appName}-service`, {
  name: `${appName}-service`,
  description: "",
  tags: {},
});

// Elastic Beanstalk Environment
export const ebEnvironment = new aws.elasticbeanstalk.Environment(
  `${appEnvironment}-${appName}`,
  {
    name: `${appEnvironment}-${appName}`,
    application: app.name,
    solutionStackName:
      "64bit Amazon Linux 2018.03 v2.16.0 running Docker 19.03.6-ce",
    settings: [
      // "Modify Security" in the console
      {
        name: "ServiceRole",
        namespace: "aws:elasticbeanstalk:environment",
        value: serviceRole.name,
      },
      {
        name: "IamInstanceProfile",
        namespace: "aws:autoscaling:launchconfiguration",
        value: instanceProfile.name,
      },
      {
        name: "InstanceType",
        namespace: "aws:autoscaling:launchconfiguration",
        value: config.require("instanceType"),
      },
      // Modify Monitoring
      {
        name: "SystemType",
        namespace: "aws:elasticbeanstalk:healthreporting:system",
        value: "enhanced", // Default - "basic"
      },
    ],
  }
);
